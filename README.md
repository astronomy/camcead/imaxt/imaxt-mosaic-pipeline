# IMAXT Mosaic Pipeline

The IMAXT mosaic pipeline produces stitched images from tile observations.
Currently we support two instruments: Serial Two-Photon Tomography (STPT)
and AXIOScan. 

The datasets are 3D volumes imaged slice by slice. Each slice is made from hundreds of tiles observed in multiple channels. The tiles are imaged in a raster scan pattern and in order to compute the 3D data volumne the pipeline carries out the following steps:

* Calibration: computes and applies the calibration images dark correction and flat field correction for the tiles.
* Geometric distortion correction: applies the geometric distortion correction to each tile.
* Offset calculation: computes the offset between the tiles for each slice.
* Stitch: Stitches the tiles using the computed offsets.
* Bead finding: Segment the beads in the stitched images and compute the bead positions.

The following figure shows a resultant 3D volume from a STPT dataset made from about 40000 tiles:

![](stpt_stitched.png)


The pipeline scales to multiple processors and nodes running in a distributed fashion.


The following figure shows the resources used during a typical run using four workers in our Kubernetes cluster (values are averaged over a window of two minutes):

![](resources.png)


## Pipeline Definition File

In order to run the pipeline you need a YAML file as follows:

```
version: 1

name: imaxt-mosaic

input_path: /storage/imaxt/raw/stpt/20211020_PDX_STG139_GFP_029621_100x15um
output_path: /storage/imaxt/processed/stpt/20211020_PDX_STG139_GFP_029621_100x15um

stitcher: stpt

recipes:
  - calibration
  - mosaic
  - beadfit

resources:
  cpu: 16
  workers: 1
  memory: 48
```

### Execute locally with Owl

In order to run the pipeline locally, install as follows:
```
pip install imaxt-mosaic owl-pipeline-client
```

and execute:

```
owl execute pipeline.yaml
```

### Remote execution

To execute the pipeline using a remote Owl server it is not needed to install
the module locally. Just submit as:

```
owl submit pipeline.yaml
```
