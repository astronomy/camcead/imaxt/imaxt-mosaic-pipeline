import json
from contextlib import suppress
from pathlib import Path
from typing import List, Tuple

import dask
import dask.array as da
import numpy as np
import xarray as xr
import zarr
from astropy.visualization import PercentileInterval
from dask import delayed
from owl_dev.logging import logger

from .utils import create_batches, trim_memory_workers


@delayed
def compute_percentile(img: np.ndarray, percentile: int = 98) -> Tuple[float, float]:
    """
    Compute the pixel values at the given percentile

    Parameters
    ----------
    img: ndarray
        The input image.
    percentile: float
        The percentile to compute. Default is 98.

    Returns
    -------
    tuple
        A tuple containing the pixel values at the given percentile.
    """
    p = PercentileInterval(percentile)
    p1, p2 = p.get_limits(img.ravel())
    return (p1, p2)


def compute_flat(arr: np.ndarray, nsample: int = 1000) -> xr.DataArray:
    """
    Compute the flat field for a stack of images.

    Parameters
    ----------
    arr : ndarray
        The input stack of images.
    nsample : int
        The maximum number of images to use when computing the flat field.
        Default is 1000.

    Returns
    -------
    DataArray
        An xarray DataArray containing the computed flat field.
    """
    flat = 0
    nimgs = 0
    maxval = 0
    nsample = min([nsample, len(arr)])
    batches = create_batches(len(arr), nsample)
    for b in batches:
        if len(b) < nsample:
            continue
        flat = flat + arr[b].sum(axis=0).compute()
        maxval = max(maxval, arr[b].max().compute())
        nimgs = nimgs + len(b)

    flat = flat / nimgs
    flat_median = np.median(flat)
    flat = flat / flat_median

    if len(arr) > nsample:
        indx = np.random.choice(len(arr), nsample, replace=False)
        arr = arr[indx]
    res = [compute_percentile(d) for d in arr]
    res = dask.compute(res)[0]
    pmax = max(r[1] for r in res)
    pmin = min(r[0] for r in res)

    ny, nx = flat.shape
    flat = xr.DataArray(
        flat[None, ...],
        dims=("channel", "y", "x"),
        coords={
            "y": range(ny),
            "x": range(nx),
            "channel": [0],
        },
    )

    flat.attrs["pval"] = (float(pmin), float(pmax))
    flat.attrs["median"] = flat_median
    flat.attrs["nimages"] = nimgs
    flat.attrs["maxval"] = float(maxval)

    return flat


def compute_dark(arr: np.ndarray, nthresh: int = 10) -> xr.DataArray:
    """
    Compute the dark field for a stack of images.

    Parameters
    ----------
    arr : ndarray
        The input stack of images.
    nthresh : int
        The number of images below which a median dark will not be calculated.
        Default is 10.

    Returns
    -------
    DataArray
        An xarray DataArray containing the computed dark field.
    """
    ni, ny, nx = arr.shape
    if ni < nthresh:
        dark = np.zeros((ny, nx))
    else:
        dark = da.median(arr, axis=0).compute()

    ny, nx = dark.shape
    dark = xr.DataArray(
        dark[None, ...],
        dims=("channel", "y", "x"),
        coords={
            "y": range(ny),
            "x": range(nx),
            "channel": [0],
        },
    )

    dark.attrs["nimages"] = ni

    return dark


def write_cals(
    flats: List[xr.DataArray], darks: List[xr.DataArray], output_path: Path
) -> xr.Dataset:
    """
    Write calibration data (flats and darks) to a zarr file format.

    Parameters
    ----------
    flats : list of xarray.DataArray
        A list of xarray.DataArray objects representing flatfield images
        for each channel.
    darks : list of xarray.DataArray
        A list of xarray.DataArray objects representing dark images
        for each channel.
    output_path : str or pathlib.Path
        The path to the directory where the calibration data will be stored.

    Returns
    -------
    res : dict
        A dictionary containing metadata about the zarr file.

    Notes
    -----
    This function concatenates the flatfield and dark images along the channel dimension
    and writes them to a zarr file with the directory name "cals" inside `output_path`.

    The following metadata attributes are added to the flatfield data array:
    - "median": a dictionary with the median value for each channel
    - "nimages": a dictionary with the number of images for each channel
    - "maxval": a dictionary with the maximum value for each channel
    - "pval": a dictionary with the percentile value for each channel

    The following metadata attribute is added to the dark data array:
    - "nimages": a dictionary with the number of images for each channel
    """
    arr_flats = xr.concat(flats, dim="channel")
    arr_flats.attrs["median"] = json.dumps(
        {int(f.channel.values): f.attrs["median"] for f in flats}
    )
    arr_flats.attrs["nimages"] = json.dumps(
        {int(f.channel.values): f.attrs["nimages"] for f in flats}
    )
    arr_flats.attrs["maxval"] = json.dumps(
        {int(f.channel.values): f.attrs["maxval"] for f in flats}
    )
    arr_flats.attrs["pval"] = json.dumps(
        {int(f.channel.values): f.attrs["pval"] for f in flats}
    )

    arr_darks = xr.concat(darks, dim="channel")
    arr_darks.attrs["nimages"] = json.dumps(
        {int(f.channel.values): f.attrs["nimages"] for f in darks}
    )

    ds = xr.Dataset()
    ds["flats"] = arr_flats
    ds["darks"] = arr_darks

    res = ds.to_zarr(output_path / "cals", mode="w", consolidated=True)
    return res


def compute_calibrations(
    input_path: Path, output_path: Path, overwrite: bool = False, max_images: int = 2000
) -> None:
    """
    Compute calibration frames from input data and save them to disk.

    Parameters
    ----------
    input_path : str or PathLike
        Path to input data in zarr format.
    output_path : str or PathLike
        Path to directory where calibration frames will be saved.
    overwrite : bool, optional
        If False, the function will not recompute the calibration frames if they already exist
        at `output_path`. Default is False.
    max_images : int, optional
        Maximum number of images to use for each channel. Default is 2000.

    Returns
    -------
    None

    Notes
    -----
    This function computes flatfield and dark frames from the input data and saves them to disk in zarr
    format. If `overwrite` is False and the calibration frames already exist at `output_path`, the
    function will not recompute them.

    The input data should have the following dimensions and coordinates:
    - channel: the channel index (e.g. "DAPI", "GFP", etc.)
    - z: the z-index (e.g. "0", "1", "2", etc.)
    - y: the y-coordinate
    - x: the x-coordinate

    The calibration frames will be saved to `output_path/cals.zarr`. The "flats" group will contain the
    flatfield frames for each channel, and the "darks" group will contain the dark frames for each channel.

    The flatfield frames are computed by selecting the images whose mean intensities are above a certain
    threshold and averaging them together. The threshold is determined as the 50th percentile of the means.
    The `compute_flat` function is used for this computation.

    The dark frames are computed by selecting the images whose mean intensities are below a certain threshold
    and averaging them together. The threshold is determined as the 0th percentile of the means.
    The `compute_dark` function is used for this computation.

    The `max_images` parameter can be used to limit the number of images used for each channel.
    This can help reduce memory usage and computation time.
    """
    if not overwrite and output_path.exists():
        with suppress(zarr.errors.PathNotFoundError):
            zarr.open(output_path / "cals", mode="r")
            logger.info("Calibrations already exist, skipping")
            return

    ds = xr.open_zarr(input_path)
    flats = []
    darks = []
    for ch in ds.channel:
        logger.debug("Processing channel %s", ch.values)
        arr = ds.sel(channel=ch, z=0).to_array().data
        ns, nt, ny, nx = arr.shape
        arr = arr.reshape((ns * nt, ny, nx))

        arr = arr[:max_images]
        nimgs, *_ = arr.shape
        nsample = min(nimgs, 500)
        logger.debug("Selecting images")
        means = [
            arr[batch].mean(axis=(1, 2)).compute()
            for batch in create_batches(nimgs, nsample)
            if len(batch) == nsample
        ]

        means = np.hstack(means)
        thresh = np.quantile(means, (1e-17, 0.5))
        arr = arr[: len(means)]

        good = means > thresh[1]
        logger.debug("Computing flatfield. Found %d good images", good.sum())
        flat = compute_flat(arr[good], nsample=min(1000, sum(good)))
        flats.append(flat.assign_coords({"channel": [ch]}))

        good = means < thresh[0]
        logger.debug("Computing dark. Found %d good images", good.sum())
        dark = compute_dark(arr[good])
        darks.append(dark.assign_coords({"channel": [ch]}))

    logger.debug("Writing calibration frames to %s", output_path)
    write_cals(flats, darks, output_path)
    trim_memory_workers()
