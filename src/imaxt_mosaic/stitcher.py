from dataclasses import dataclass

import dask
import numpy as np
import pandas as pd
from dask import delayed
from dask.distributed import get_client, wait
from sklearn.covariance import EllipticEnvelope

from .settings import Settings
from .stitchlib.optimization import (
    compute_final_position,
    compute_maximum_spanning_tree,
)
from .stitchlib.refinement import find_local_max_integer_constrained
from .stitchlib.stage_model import (
    compute_image_overlap2,
    filter_by_overlap_and_correlation,
    filter_by_repeatability,
    filter_outliers,
    replace_invalid_translations,
)
from .stitchlib.translation import calculate_translation


@dataclass
class EllipticEnvelopPredictor:
    """Anomaly detector based on the elliptic envelope algorithm.

    This class applies the elliptic envelope algorithm to detect anomalies in a
    dataset. It assumes that the data follows an elliptical distribution, and
    fits a minimum volume enclosing ellipsoid (MVEE) to the inliers of the data.
    Data points that fall outside the MVEE are considered to be outliers.

    Parameters
    ----------
    contamination : float
        The expected proportion of outliers in the dataset. This parameter is
        used to set the threshold for classifying samples as outliers.
    epsilon : float
        The amount of noise to add to the data to help regularize the MVEE.
    random_seed : int
        The random seed to use for generating the noise.

    Returns
    -------
    y_pred : ndarray, shape (n_samples,)
        The predicted class labels, where 1 indicates an inlier and -1 indicates
        an outlier.

    Notes
    -----
    The elliptic envelope algorithm assumes that the data follows an elliptical
    distribution. If the data does not conform to this assumption, the algorithm
    may not be effective at detecting outliers.

    Examples
    --------
    >>> from numpy.random import default_rng
    >>> from sklearn.datasets import make_blobs
    >>> from mymodule import EllipticEnvelopPredictor
    >>> rng = default_rng(0)
    >>> X, _ = make_blobs(n_samples=1000, centers=1, cluster_std=0.5, random_state=0)
    >>> X[-10:] = rng.uniform(low=-10, high=10, size=(10, 2))
    >>> ee = EllipticEnvelopPredictor(contamination=0.05, epsilon=0.01, random_seed=0)
    >>> y_pred = ee(X)
    >>> print(y_pred)
    [ True  True  True ...  True False False]
    """

    contamination: float
    epsilon: float
    random_seed: int

    def __call__(self, X):
        ee = EllipticEnvelope(contamination=self.contamination)
        rng = np.random.default_rng(self.random_seed)
        X = rng.normal(size=X.shape) * self.epsilon + X
        try:
            return ee.fit_predict(X) > 0
        except Exception:
            return np.ones(X.shape[0], "int8") > 0


class Stitcher:
    def __init__(self, images, stagepos, downsample=True):
        self.images = images
        self.sp = stagepos
        self.downsample = downsample
        try:
            workers = dask.config.config["annotations"]["workers"]
            self.workers = workers or None
        except KeyError:
            self.workers = None

    def compute_first_pass(self, images, grid, metric="structural_similarity"):
        """Compute the first pass of image registration.

        This function performs the first pass of image registration using a grid of image indices and a similarity metric.
        For each pair of adjacent images in the grid, the function calculates the translation that maximizes the similarity
        metric between the images. The translations are stored in the `grid` dataframe as columns with names ending in
        "_ncc_first", "_y_first", and "_x_first", where "ncc" is the similarity metric used (normalized cross-correlation
        or structural similarity index), "y" is the translation in the y direction, and "x" is the translation in the x
        direction.

        Parameters
        ----------
        images : numpy.ndarray
            A 3D numpy array with shape (num_images, height, width) containing the input images.
        grid : pandas.DataFrame
            A pandas dataframe containing the indices of the images in the grid and the initial guesses for the
            translations. The dataframe should have columns "left", "top", "left_y_init_guess", "left_x_init_guess",
            "top_y_init_guess", and "top_x_init_guess". The "left" and "top" columns contain the indices of the adjacent
            images in the grid, and the "_y_init_guess" and "_x_init_guess" columns contain the initial guesses for the
            translations in the y and x directions, respectively. If the initial guesses are not available, the corresponding
            values should be set to NaN.
        metric : str, optional
            The similarity metric to use. Valid values are "normalized_cross_correlation" (default) and "structural_similarity".

        Returns
        -------
        pandas.DataFrame
            The input `grid` dataframe with the translations stored in columns ending in "_ncc_first", "_y_first", and "_x_first".

        Raises
        ------
        ValueError
            If the `metric` parameter is not valid.
        """
        res = []
        k = 0
        for direction in ["left", "top"]:
            for i2, g in grid.iterrows():
                i1 = g[direction]
                if pd.isna(i1):
                    continue
                image1 = images[i1]
                image2 = images[i2]
                ny, nx = image1.shape

                if self.sp.position_initial_guess is not None:

                    def get_lims(dimension, size):
                        val = g[f"{direction}_{dimension}_init_guess"]
                        r = size * self.sp.overlap_diff_threshold / 100.0
                        return np.round([val - r, val + r]).astype(np.int64)

                    lims = np.array(
                        [
                            get_lims(dimension, size)
                            for dimension, size in zip("yx", [ny, nx])
                        ]
                    )
                else:
                    lims = np.array([[-ny, ny], [-nx, nx]])

                max_peak = delayed(calculate_translation)(
                    image1, image2, lims, metric=metric
                )
                k += 1
                res.append(max_peak)

        client = get_client()
        # secede()
        max_peak = [client.compute(r, workers=self.workers) for r in res]
        wait(max_peak)
        # rejoin()
        max_peak = [m.result() for m in max_peak]
        max_peak_threshold = np.percentile(
            [m[0] for m in max_peak], Settings.max_peak_percentile
        )
        scl = 0.5 / max_peak_threshold
        max_peak = [(m[0] * scl, m[1], m[2]) for m in max_peak]

        k = 0
        for direction in ["left", "top"]:
            for i2, g in grid.iterrows():
                i1 = g[direction]
                if pd.isna(i1):
                    continue
                for j, key in enumerate(["ncc", "y", "x"]):
                    grid.loc[i2, f"{direction}_{key}_first"] = max_peak[k][j]
                k += 1

        return grid

    def filter_grid(self, images, grid, pou=3):
        """
        Filters the input `grid` based on the quality of the computed translations between pairs of images in `images`.

        Parameters
        ----------
        images : list of ndarrays
            A list of input images.
        grid : pandas DataFrame
            A pandas DataFrame representing the grid containing the computed translations between pairs of images.
        pou : float, optional
            Percentage of overlap between images allowed for a pair of images to be considered valid.

        Returns
        -------
        pandas DataFrame
            A filtered grid that contains only the pairs of images with high quality translations.
        """
        sizeY, sizeX = images[0].shape
        predictor = EllipticEnvelopPredictor(
            contamination=0.4, epsilon=0.01, random_seed=0
        )
        left_displacement = compute_image_overlap2(
            grid[grid["left_ncc_first"] > 0.5], "left", sizeY, sizeX, predictor
        )
        top_displacement = compute_image_overlap2(
            grid[grid["top_ncc_first"] > 0.5], "top", sizeY, sizeX, predictor
        )

        overlap_top = np.clip(100 - top_displacement[0] * 100, pou, 100 - pou)
        overlap_left = np.clip(100 - left_displacement[1] * 100, pou, 100 - pou)

        # compute_repeatability
        grid["top_valid1"] = filter_by_overlap_and_correlation(
            grid["top_y_first"], grid["top_ncc_first"], overlap_top, sizeY, pou
        )
        grid["top_valid2"] = filter_outliers(grid["top_y_first"], grid["top_valid1"])
        grid["left_valid1"] = filter_by_overlap_and_correlation(
            grid["left_x_first"], grid["left_ncc_first"], overlap_left, sizeX, pou
        )
        grid["left_valid2"] = filter_outliers(grid["left_x_first"], grid["left_valid1"])

        rs = []
        for direction, dims, rowcol in zip(
            ["top", "left"], ["yx", "xy"], ["col", "row"]
        ):
            valid_key = f"{direction}_valid2"
            valid_grid = grid[grid[valid_key]]
            if len(valid_grid) > 0:
                w1s = valid_grid[f"{direction}_{dims[0]}_first"]
                r1 = np.ceil((w1s.max() - w1s.min()) / 2)
                _, w2s = zip(
                    *valid_grid.groupby(rowcol)[f"{direction}_{dims[1]}_first"]
                )
                r2 = np.ceil(np.max([np.max(w2) - np.min(w2) for w2 in w2s]) / 2)
                rs.append(max(r1, r2))
            rs.append(0)
        self.r = np.max(rs)

        grid = filter_by_repeatability(grid, self.r)
        grid = replace_invalid_translations(grid)
        return grid

    def compute_second_pass(self, images, grid):
        """
        Compute second-pass feature matching between images in a grid.

        Parameters
        ----------
        images : list of numpy.ndarray
            A list of grayscale images.
        grid : pandas.DataFrame
            A DataFrame containing the grid information, as generated by the
            `compute_first_pass` function.

        Returns
        -------
        pandas.DataFrame
            The updated grid DataFrame with the refined feature positions and NCC
            values computed during the second pass.

        Notes
        -----
        This function refines the feature positions of the matching pairs of images
        in the input grid, by performing a local maximum search around the initial
        values computed during the first pass. The search is constrained by the
        maximum displacement radius `self.r`.

        For each image pair and displacement direction, the function spawns a
        delayed Dask task to perform the local maximum search, and collects the
        results asynchronously. The updated positions and NCC values are then
        stored in the grid DataFrame.

        """
        res = []
        for direction in ["left", "top"]:
            for i2, g in grid.iterrows():
                i1 = g[direction]
                if pd.isna(i1):
                    continue
                image1 = images[i1]
                image2 = images[i2]
                sizeY, sizeX = image1.shape

                init_values = [
                    int(g[f"{direction}_y_second"]),
                    int(g[f"{direction}_x_second"]),
                ]
                limits = [
                    [
                        max(-sizeY + 1, init_values[0] - self.r),
                        min(sizeY - 1, init_values[0] + self.r),
                    ],
                    [
                        max(-sizeX + 1, init_values[1] - self.r),
                        min(sizeX - 1, init_values[1] + self.r),
                    ],
                ]
                v = delayed(find_local_max_integer_constrained)(
                    image1, image2, np.array(init_values), np.array(limits)
                )

                res.append(v)

        out = dask.compute(res)[0]

        k = 0
        for direction in ["left", "top"]:
            for i2, g in grid.iterrows():
                i1 = g[direction]
                if pd.isna(i1):
                    continue
                values, ncc_value = out[k]
                grid.loc[i2, f"{direction}_y"] = values[0]
                grid.loc[i2, f"{direction}_x"] = values[1]
                grid.loc[i2, f"{direction}_ncc"] = ncc_value
                k += 1

        return grid

    def compute_final_pass(self, grid):
        """
        Compute final feature matching between images in a grid.

        Parameters
        ----------
        grid : pandas.DataFrame
            A DataFrame containing the grid information, as generated by the
            `compute_sencond_pass` function.

        Returns
        -------
        pandas.DataFrame
            The updated grid DataFrame with the refined feature positions and NCC
            values computed during the second pass.
        """
        tree = compute_maximum_spanning_tree(grid)
        grid = compute_final_position(grid, tree)
        return grid
