import dask.array as da
import pandas as pd
import xarray as xr
from dask import delayed
from owl_dev.logging import logger
import cv2
import numpy as np
from ..settings import Settings
from ..utils import restart_workers, trim_memory_workers
from .bead_detector import (
    bead_detect_worker,
    create_mask,
    plot_beads,
    wmean_image,
    get_headname,
)


def bead_detect(output_path, sampling_group, overwrite=False, fig=False):
    """Detect beads in a given image."""
    # preparing input/output directories
    mod_input_path = output_path / "mos"
    _bead_dir = output_path / "bead"
    _bead_dir.mkdir(exist_ok=True)

    _mask_dir = _bead_dir / "mask"
    _mask_dir.mkdir(exist_ok=True)

    ds = xr.open_zarr(mod_input_path)
    ds_lx = xr.open_zarr(mod_input_path, group=sampling_group)
    sections = list(ds)
    if Settings.sections:
        sections = [sections[i - 1] for i in Settings.sections]

    logger.info(f"{len(sections)} section(s) to proceed with")

    for sect in sections:
        output_bead = _bead_dir / f"{sect}.parquet"
        if output_bead.exists() and not overwrite:
            logger.info(f"Skipping section {sect}")
            continue
        logger.info(f"Processing section {sect}")

        # loop over all available z planes
        all_bead_z = []
        for iz_sect, z_sect in enumerate(ds[sect]["z"].data):
            mean_data_org = ds[sect].sel(z=z_sect).mean("channel").data
            # for the mask, we prefer to use sum
            # Update 17 April 2023. We used weighted median instead of sum
            # mean_data_lx = ds_lx[sect].sel(z=z_sect).sum("channel").data

            # extract the target headname from the meta file
            meta_lx = ds_lx[sect].sel(z=z_sect).raw_meta
            targs_headname = get_headname(meta_lx)

            mean_data_lx = wmean_image(ds_lx[sect].sel(z=z_sect))

            # create a mask from the scaled image
            # A- mask with similar lx dimensions
            # first we estimate cl_morph param based on 1% of the input image size
            cl_morph_parm = np.ceil(np.nanmean(list(mean_data_lx.shape)) * 0.01).astype(
                int
            )

            mask_image_lx_d = delayed(create_mask)(
                mean_data_lx,
                targs_headname=targs_headname,
                cl_morph_parm=cl_morph_parm,
                c_min_area=0.0009,
                c_count_max=15,
                output_size=None,
                return_BS_lx=True,
            )

            mask_image_lx = da.from_delayed(
                mask_image_lx_d[0], shape=mean_data_lx.shape, dtype="uint8"
            ).rechunk(mean_data_lx.chunksize)

            BS_lx = da.from_delayed(
                mask_image_lx_d[1], shape=mean_data_lx.shape, dtype="uint8"
            ).rechunk(mean_data_lx.chunksize)

            # B- mask with similar original data dimensions
            mask_image_d = delayed(create_mask)(
                mean_data_lx,
                targs_headname=targs_headname,
                cl_morph_parm=cl_morph_parm,
                c_min_area=0.0009,
                c_count_max=15,
                output_size=mean_data_org.shape[::-1],
                return_BS_lx=False,
            )

            mask_image = da.from_delayed(
                mask_image_d[0], shape=mean_data_org.shape, dtype="uint8"
            ).rechunk(mean_data_org.chunksize)

            beads_df = bead_detect_worker(
                mean_data_org,
                mask_image,
                mean_data_lx,
                mask_image_lx,
                overlap_size=200,
            )
            beads_df["section"] = str(sect)
            beads_df["z"] = z_sect
            all_bead_z.append(beads_df)

            if fig:
                plot_beads(
                    mean_data_org,
                    mask_image,
                    beads_df,
                    fig_fname=_bead_dir / f"{sect}_z{z_sect}_bead.png",
                )
            # store the original and lx mask and BS in a npz and png file
            output_mask_png = _mask_dir / f"mask_{sect}_z{z_sect}.png"
            output_mask_npz = _mask_dir / f"mask_{sect}_z{z_sect}.npz"
            output_BS_png = _mask_dir / f"bs_{sect}_z{z_sect}.png"
            output_BS_npz = _mask_dir / f"bs_{sect}_z{z_sect}.npz"

            cv2.imwrite(str(output_mask_png), np.array(mask_image_lx) * 255)
            np.savez_compressed(str(output_mask_npz), np.array(mask_image_lx))
            cv2.imwrite(str(output_BS_png), np.array(BS_lx))
            np.savez_compressed(str(output_BS_npz), np.array(BS_lx))

            del mask_image
            del mean_data_org
            del mean_data_lx
            del BS_lx

        df = pd.concat(all_bead_z)
        df.to_parquet(output_bead)
        logger.info(
            "Bead detection done for section %s:Z%d (%d)", sect, z_sect, len(df)
        )

        trim_memory_workers()
        restart_workers()


# TODO: write as script
# if __name__ == "__main__":
