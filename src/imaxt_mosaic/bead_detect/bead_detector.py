import itertools
import sys
import traceback

import cv2
import dask
import dask.array as da
import numpy as np
import pandas as pd
import scipy.ndimage as ndi
import sep
from dask import delayed
from owl_dev.logging import logger

from ..typing_utils import NumArray


def get_headname(meta):
    """
    A dummy function to extract the headname from the meta file
    """
    meta_axio = np.array([meta.find(x) for x in ["AXIO", "axio", "Axio"]])
    meta_stpt = np.array([meta.find(x) for x in ["STPT", "stpt", "Stpt"]])

    if meta_axio[meta_axio > -1].sum() > 0:
        return "AXIO"
    elif meta_axio[meta_stpt > -1].sum() > 0:
        return "STPT"
    else:
        return "TARGS"


def wmean_image(data_lx):
    # generate weighted average of channels.
    # weights are based on (std/mean)**2 in each channel
    ch_list = list(data_lx.channel.to_numpy())
    ch_array = []
    for chl in ch_list:
        chl_i = data_lx.sel(channel=chl)
        ch_array.append(chl_i / chl_i.mean())
    image = dask.array.median(dask.array.asarray(ch_array), axis=0)
    del chl_i
    del ch_array
    return image


def create_mask(
    image: NumArray,
    targs_headname="targs",
    cl_morph_parm=5,
    c_min_area=0.0009,
    c_count_max=15,
    output_size=None,
    return_BS_lx=False,
):

    """Create mask from the scaled image

    Parameters
    ----------
    image
        input image (better to be an scaled image)
    cl_morph_parm
        cv2.MORPH_ELLIPSE parameter for the Morphological closing
    c_min_area
        min area (relative to the full field area) for a contour to be considered as mask
    c_count_max
        Maximum number of contours to be checked for being mask
    output_size
        output image size

    Returns
    -------
    output
        Mask image with mask values set to 1 and background values set to 0
    """

    # to avoid compressing all background values around 0, and dealing with
    image = cv2.normalize(image, None, 1, 255, cv2.NORM_MINMAX, -1).astype(np.float32)

    if return_BS_lx:
        # keep a copy of image (lx) generated as this stage to generate BS_lx
        _image_lx = (image.astype(np.uint8)).copy()

    # apply close morphology to get rid of small structures and make the big blog bolder
    # change the kernel to (5,5) if background is still noisy

    image = cv2.blur(image, (3, 3))

    outer = cv2.morphologyEx(
        image,
        cv2.MORPH_CLOSE,
        cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (cl_morph_parm, cl_morph_parm)),
    )

    # fill/replace zeros with median value to have smoother background
    # then we apply blur filter to remove any artificial casued by filling with median
    # for stpt we usually have smaller background and the main field is mostly covered with tissue
    if targs_headname.upper() in ["STPT", "ST", "S"]:
        bkg_value = np.nanquantile(outer[outer > 1], 0.2)
    elif targs_headname.upper() in ["AXIO", "AX", "A"]:
        bkg_value = np.nanquantile(outer[outer > 1], 0.5)
    else:
        bkg_value = np.nanquantile(outer[outer > 1], 0.5)

    outer[outer <= 1] = bkg_value
    outer = outer.astype(np.uint8)
    outer = cv2.blur(outer, (5, 5))

    # remove noise by filtering the most frequent element from the image
    ret, th1 = cv2.threshold(
        outer, bkg_value, 255, cv2.THRESH_BINARY + cv2.THRESH_TRIANGLE
    )

    # binaries the image and fill the holes
    image_fill_holes = ndi.binary_fill_holes(th1).astype(np.uint8)

    cnts, _ = cv2.findContours(
        image_fill_holes, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
    )

    # create an empty mask image
    mask = np.zeros(image.shape, np.uint8)

    # assuming the min area for tissue is (e.g. 0.0009 = 0.03x * 0.03y)
    min_blob_area = c_min_area * image.shape[1] * image.shape[0]
    blob_counter = 0

    # sort contours by area (large to small) and then loop over them and select those with specific characteristics
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    for cnt in cnts[: np.min([int(c_count_max), len(cnts)])]:
        if cv2.contourArea(cnt) > min_blob_area:
            blob_counter += 1
            cv2.drawContours(mask, [cnt], -1, 255, cv2.FILLED)
            mask = cv2.bitwise_and(image_fill_holes, mask)

    # delete redundant
    del image
    del outer

    if return_BS_lx:
        BS_lx = _image_lx * mask

    if output_size is not None:
        mask = cv2.resize(mask, dsize=output_size, interpolation=cv2.INTER_LINEAR)

    if return_BS_lx:
        return (mask, BS_lx)
    else:
        return (mask, None)


def run_sep(
    indx,
    im,
    mask,
    overlap_size=200.0,
    chunk_loc=None,
    bw=128,
    bh=128,
    minarea=880,
    deblend_nthresh=2,
    circularity=0.9,
    std_lx=None,
):

    if chunk_loc is None:
        chunk_loc = [[np.nan, np.nan], [np.nan, np.nan]]

    im = np.array(im)
    mask = np.array(mask).astype(bool)

    extc_bkg = sep.Background(im, mask=mask, bw=bw, bh=bh)

    logger.debug(
        "Background: %f - Global RMS: %f", extc_bkg.globalback, extc_bkg.globalrms
    )

    # subtract the background
    reduced_data = im - extc_bkg.back()

    # to avoid RuntimeWarning we first check the length of the following array
    if len(reduced_data[np.invert(mask)]) > 0:
        thres_base = max(0, np.nanmean(reduced_data[np.invert(mask)]))
    else:
        thres_base = 0.0

    # here you could introduce as many detection-thresholds as you want to give a try
    threshold_0 = thres_base + (1.3 * np.std(extc_bkg.back()))
    threshold_1 = thres_base + (1.8 * np.std(extc_bkg.back()))
    threshold_2 = thres_base + (2.3 * np.std(extc_bkg.back()))
    threshold_3 = thres_base + (3.0 * np.std(extc_bkg.back()))
    threshold_4 = thres_base + (1.5 * std_lx)
    threshold_list = [threshold_0, threshold_1, threshold_2, threshold_3, threshold_4]

    try:
        for i_th, thr_i in enumerate(threshold_list):
            try:
                # detect objects (try 1): Using the local (block) bacground

                objects = sep.extract(
                    reduced_data,
                    thr_i,
                    minarea=minarea,
                    deblend_nthresh=deblend_nthresh,
                    mask=mask,
                    segmentation_map=False,
                )
                break
            except Exception:
                objects = None
                if i_th < len(threshold_list) - 1:
                    continue
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
        logger.debug("".join(lines))
        return None

    if objects is None:
        logger.debug("No objects found")
        return pd.DataFrame()

    objects_df = pd.DataFrame(objects)

    objects_df = objects_df[
        (objects_df["flag"] == 0) & (objects_df["b"] / objects_df["a"] > circularity)
    ]

    # add indx and x(y)_range of each chunk to the dataframe
    df_len = len(objects_df)
    objects_df.insert(0, "indx_0", indx[0])
    objects_df.insert(1, "indx_1", indx[1])
    objects_df.insert(2, "X_RANGE_i", chunk_loc[1][0])
    objects_df.insert(3, "X_RANGE_f", chunk_loc[1][1])
    objects_df.insert(4, "Y_RANGE_i", chunk_loc[0][0])
    objects_df.insert(5, "Y_RANGE_f", chunk_loc[0][1])
    objects_df.rename({"x": "x_block", "y": "y_block"}, axis=1, inplace=True)

    # estimate absolute coordinates of the peaks and keep them as new columns
    # called x and y (also corrected for overlapping)
    # Here we use (2 * index +1) * overlap_size factor to convert coordinates
    # from overlapped to the original (non-overlapped)

    if df_len > 0:

        fn_x = (
            lambda row: row.X_RANGE_i
            + row.x_block
            - (2.0 * row.indx_1 + 1) * overlap_size
        )

        col_x = objects_df.apply(fn_x, axis=1)
        objects_df = objects_df.assign(x=col_x.values)

        fn_y = (
            lambda row: row.Y_RANGE_i
            + row.y_block
            - (2.0 * row.indx_0 + 1) * overlap_size
        )

        col_y = objects_df.apply(fn_y, axis=1)
        objects_df = objects_df.assign(y=col_y.values)

        # TODO: more accurate way to estimate the radius
        fn_r = (lambda row: np.sqrt(row.a ** 2 + row.b ** 2) / 2)
        col_r = objects_df.apply(fn_r, axis=1)
        objects_df = objects_df.assign(r=col_r.values)

    else:
        objects_df["x"] = []
        objects_df["y"] = []
        objects_df["r"] = []

    logger.debug("Number of detected objects: %s (try: %d)", df_len, i_th + 1)

    return objects_df


@delayed
def compute_background_lx(image_lx: NumArray, mask_lx: NumArray, bw=128, bh=128):
    try:
        lx_bkg = sep.Background(image_lx, mask=mask_lx, bw=128, bh=128)
        std_lx = np.std(lx_bkg.back())
    except Exception:
        std_lx = np.nanquantile(image_lx[mask_lx == 0], 0.75)
    return std_lx


def bead_detect_worker(image, mask, image_lx, mask_lx, overlap_size=0.0):

    # Evaluate standard deviation of background extracted from lx
    # In case the original background failed, we deploy this value
    std_lx = compute_background_lx(image_lx, mask_lx, bw=128, bh=128)

    if overlap_size > 0.0:
        # add overlap to data and mask (for mask, we refill the boundary with 1 [equivalent to mask=True])
        image = da.overlap.overlap(image, overlap_size, {0: 1, 1: 1})
        mask = da.overlap.overlap(mask, overlap_size, {0: 1, 1: 1})

    beads_df = []
    for indx in itertools.product(*map(range, image.blocks.shape)):

        # extract the location of each chunk (overlapped) and later,
        # pass it to the run_sep function to be included in the results
        i, j = indx[-2:]
        chunk_loc_indx = [
            [sum(image.chunks[-2][:i]), sum(image.chunks[-2][: i + 1])],
            [sum(image.chunks[-1][:j]), sum(image.chunks[-1][: j + 1])],
        ]

        circ_chunk_i = delayed(run_sep)(
            indx,
            image.blocks[indx],
            mask.blocks[indx],
            overlap_size=overlap_size,
            chunk_loc=chunk_loc_indx,
            std_lx=std_lx,
        )

        if circ_chunk_i is not None:
            beads_df.append(circ_chunk_i)

    final_beads = dask.compute(beads_df)[0]
    final_beads = pd.concat(final_beads, ignore_index=True)

    return final_beads


def plot_beads(image, mask, beads_df, fig_fname=None):

    try:
        import matplotlib

        matplotlib.use("Agg")
        import matplotlib.pyplot as plt
        from matplotlib.patches import Ellipse
    except ModuleNotFoundError:
        logger.warning("matplotlib not found, cannot plot beads")
        return

    fig = plt.figure(figsize=(12, 8))

    ax1 = fig.add_subplot(1, 2, 1)
    ax1.imshow(np.array(mask), cmap="gray")
    ax1.set_title("Mask image")

    ax2 = fig.add_subplot(1, 2, 2)
    image = np.array(image)

    m, s = np.mean(image), np.std(image)
    ax2.imshow(image, interpolation="nearest", cmap="gray", vmin=m - s, vmax=m + s)
    ax2.set_title("Detected Beads")
    # plot an ellipse for each object
    for i in range(len(beads_df)):
        try:
            # plot boarder or each ellipse
            e = Ellipse(
                xy=(beads_df["x"][i], beads_df["x"][i]),
                width=6 * beads_df["a"][i],
                height=6 * beads_df["b"][i],
                angle=beads_df["theta"][i] * 180.0 / np.pi,
            )
            e.set_facecolor("none")
            e.set_edgecolor("red")
            ax2.add_artist(e)

            # mark the centre of each ellipse
            e_cent = Ellipse(
                xy=(beads_df["x"][i], beads_df["x"][i]),
                width=0.5 * beads_df["a"][i],
                height=0.5 * beads_df["b"][i],
                angle=beads_df["theta"][i] * 180.0 / np.pi,
            )
            e_cent.set_facecolor("none")
            e_cent.set_edgecolor("blue")
            ax2.add_artist(e_cent)

        except Exception:
            pass

    if fig_fname is not None:
        plt.tight_layout()
        plt.savefig(fig_fname)
        plt.close()

    else:
        plt.show()


# TODO: write as script
# if __name__ == "__main__":
