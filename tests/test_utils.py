import dask.array as da
import distributed
from imaxt_mosaic import utils


def test_create_batches():
    ntotal = 10
    nbatch = 2
    batches = utils.create_batches(ntotal, nbatch)
    assert len(batches) == 5
    assert batches[0] == [0, 1]
    assert batches[1] == [2, 3]
    assert batches[2] == [4, 5]
    assert batches[3] == [6, 7]
    assert batches[4] == [8, 9]


def test_get_coords():
    res = utils.get_coords((1000, 1000), (0, 0, 0, 0), (0, 0, 0, 0), (500, 500), 1, 1)
    assert res == (1000, 1000)

    res = utils.get_coords((1, 1), (1, 0, 0, 0), (1, 0, 0, 0), (500, 500), 1, 1)
    assert res == (0, 0)


def test_downsample():
    arr = da.zeros((10, 1000, 1000))
    arr2 = utils.downsample(arr)
    assert arr2.shape == (10, 500, 500)


def test_workers():
    def main():
        workers_info = utils.get_workers_info()
        assert isinstance(workers_info, dict)
        assert len(workers_info.keys()) == 1
        assert list(workers_info.values())[0]["status"] == "running"
        assert list(workers_info.values())[0]["type"] == "Worker"

        workers = utils.get_workers_address()
        assert isinstance(workers, list)
        assert len(workers) == 1

        # arr = da.zeros((1000, 1000))
        # arr = utils.persist_array(arr)
        # assert arr.shape == (1000, 1000)

    with distributed.LocalCluster(
        scheduler_port=0,
        dashboard_address=":0",
        asynchronous=False,
        n_workers=1,
        nthreads=1,
        processes=False,
    ) as cluster:
        with distributed.Client(cluster, asynchronous=False):
            main()
